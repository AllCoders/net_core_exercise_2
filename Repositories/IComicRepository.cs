﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Excercise2Starter.Entities;

namespace Excercise2Starter.Repositories
{
    /// <summary>
    /// I HAVE NO IMPLEMENTATION
    /// </summary>
    public interface IComicRepository
    {
        Task<List<Comic>> GetAllComicsAsync();
        Task<Comic> GetComicByIdAsync(Guid id);
    }
}
