﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Excercise2Starter.Context;
using Excercise2Starter.Entities;
using Microsoft.EntityFrameworkCore;

namespace Excercise2Starter.Repositories
{
    public class ComicRepository
    {
        private readonly ComicContext _context;

        public ComicRepository(ComicContext context)
        {
            _context = context;
        }

        public async Task<List<Comic>> GetAllComicsAsync()
        {
            return await _context.Comics.ToListAsync();
        }

        public async Task<Comic> GetComicByIdAsync(Guid id)
        {
            return await _context.Comics.SingleAsync(c => c.Id == id);
        }
    }
}
