﻿using System;
using System.Threading.Tasks;
using Excercise2Starter.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Excercise2Starter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComicsController : ControllerBase
    {
        private readonly IComicRepository _comicRepository;

        public ComicsController(IComicRepository comicRepository)
        {
            _comicRepository = comicRepository;
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok("I_SHOULD_RETURN_SOMETHING_BETTER_THAN_THIS_TEXT");
        }

        // GET api/values/5
        [HttpGet("{id:guid}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var comic = await _comicRepository.GetComicByIdAsync(id);
            return Ok(comic);
        }
    }
}
