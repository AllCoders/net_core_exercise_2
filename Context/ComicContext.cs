﻿using Excercise2Starter.Entities;
using Microsoft.EntityFrameworkCore;

namespace Excercise2Starter.Context
{
    public class ComicContext : DbContext
    {
        public ComicContext(DbContextOptions<ComicContext> options) : base(options)
        {
            Database.Migrate();
        }

        public DbSet<Comic> Comics { get; set; }
    }
}
