﻿using Excercise2Starter.Context;
using Excercise2Starter.Repositories;
using Excercise2Starter.Settings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Excercise2Starter
{
    public class Startup
    {
        // Damnit, I forgot the remove the namespaces
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = "IM_IN_THE_CONFIG";

            // Registering the Context so it can be resolved
            services.AddDbContext<ComicContext>(o => o.UseSqlServer(connectionString));

            // Migrating the database automatically with the latest changes
            // Because we work code first, in this case it will create the database for you
            services.BuildServiceProvider().GetService<ComicContext>().Database.Migrate(); // Equals => dotnet ef database update

            // To add new migrations, make the changes to your context and execute the following dotnet command
            // dotnet ef migrations add {NAME_OF_YOUR_MIGRATION}
            // After creation the migration can be found in the Migrations folder of your project

            // TIP: Use SQL Server Object Explorer in Visual Studio to view the DB
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ComicContext comicContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            comicContext.EnsureSeed(env.WebRootPath);

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Our Comic service api is running!");
            });
        }
    }
}
